from flask import Flask, request
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS, cross_origin
from os import environ
import json

app = Flask(__name__)
cors = CORS(app)

DATABASE_URL = environ.get('DATABASE_URL')
if DATABASE_URL is None:
    raise Exception('DATABASE_URL not set')

app.config['SQLALCHEMY_DATABASE_URI'] = DATABASE_URL

db = SQLAlchemy(app)

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), unique=True, nullable=False)

    def __init__(self, username):
        self.username = username

with app.app_context():
    db.create_all()

@app.route('/user', methods=['POST'])
def create_user():
    username = request.json['username']
    user = User(username=username)
    db.session.add(user)
    db.session.commit()
    return json.dumps({'id': user.id})

@app.route('/user/<int:id>', methods=['DELETE'])
def delete_user(id):
    User.query.filter_by(id=id).delete()
    db.session.commit()
    return f"User with id: {id} deleted."

@app.route('/users', methods=['GET'])
def list_users():
    users = User.query.all()
    return json.dumps([{"id": user.id, "username": user.username} for user in users]), 200

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5000)

